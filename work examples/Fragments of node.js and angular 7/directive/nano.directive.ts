import {AfterViewInit, Directive, ElementRef} from "@angular/core";

declare var jquery: any;
declare var $: any;

@Directive({
  selector: '[appNano]'
})
export class NanoDirective implements AfterViewInit {

  constructor(private elementRef: ElementRef) {
    jQuery(document).ready( ()=> {
      $(this.elementRef.nativeElement).nanogallery2({
        galleryDisplayMode: 'fullContent',
        thumbnailHeight: 150,
        thumbnailWidth: 'auto',
        itemsBaseURL: 'img/',
        thumbnailBorderHorizontal: 0,
        thumbnailBorderVertical: 0,
        thumbnailAlignment: 'left',
        galleryDisplay: 'pagination',
        galleryResizeAnimation: 'true',
        colorScheme: 'light',
        thumbnailDisplayTransition: 'fadeIn',
        thumbnailDisplayTransitionDuration: 2000,
        items: [{src: 'http://localhost:3000/user_data_post/images/1SJmGScreenshot_1.png', srct:'http://localhost:3000/user_data_post/images/1SJmGScreenshot_1.png'}]
      });
    });
  }

  ngAfterViewInit(): void {

  }
}
