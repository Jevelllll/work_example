import {Directive, ElementRef, HostListener, Input, Renderer2, ViewChild} from '@angular/core';

@Directive({
    selector: '[appActiveColor]'
})
export class ActiveColorDirective {

    @Input() public appAutoFocus: boolean;
    constructor(private _elementRef: ElementRef, private renderer: Renderer2) {
    }

    @HostListener('click', ['$event'])
    clickEvent(event) {

        const parenTLi = event.srcElement.parentElement;
        const parentUl = parenTLi.parentElement;

        let allA = [].slice.call(parentUl.querySelectorAll('a'));
        allA.map(t => {
            this.renderer.setStyle(t, 'color', '#fff');
        });
        this.renderer.setStyle(event.srcElement, 'color', '#495057');
    }


}
