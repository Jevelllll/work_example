const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const path = require('path');
const http = require('http');
const app = express();
const cors = require('cors');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mysql = require('mysql');
const myConnection = require('express-myconnection');
const dbOptionsSettings = require('./settings/dbOptions');
const users = require('./server/routes/api');
const skills = require('./server/routes/skills');
const chatRoom = require('./server/routes/chatroom');
const friend = require('./server/routes/friends');
const file_youself = require('./server/routes/file_yourself');
const user_post = require('./server/routes/user-post');
const subscribeR = require('./server/routes/subscribe');
const my_notes = require('./server/routes/my_notes');
let JWT_S = function (req, res, next) {
  req.JWT_Secret = 'qwert123';
  next();
};

app.use(myConnection(mysql, dbOptionsSettings.optionsDb, 'pool'));

app.use(cors());
app.use(fileUpload());
app.use(function (req, res, next) {
  req.jwt = jwt;
  next();
});
app.use(function (req, res, next) {
  req.bcrypt = bcrypt;
  next();
});
app.use(bodyParser.json());
/**
 * @return {string}
 */
app.use(JWT_S);
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'dist/social')));
app.use(express.static(path.join(__dirname, 'server/public')));
app.use(express.static(__dirname + '/public'));
app.use('/api/user', users);
app.use('/api/skills', skills);
app.use('/api/chat', chatRoom);
app.use('/api/friend', friend);
app.use('/api/your-self', file_youself);
app.use('/api/user-post', user_post);
app.use('/api/subscribe', subscribeR);
app.use('/api/my-notes', my_notes);
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/social/index.html'));
});
const port = process.env.PORT || '3000';
app.set('port', port);
const server = http.createServer(app);



require('./server/socket/sokets')(server);


server.listen(port, () => console.log(`Running on localhost: ${port}`));

