import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchPipe} from './search.pipe';
import {DomsanitPipe} from './domsanit.pipe';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
      SearchPipe,
      DomsanitPipe
    ],
    exports: [
      SearchPipe,
      DomsanitPipe
    ]
})

export class ExpPipeModule {}
