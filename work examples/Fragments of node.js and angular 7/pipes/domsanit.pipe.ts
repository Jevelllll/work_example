import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'domsanit'
})
export class DomsanitPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(content, type) {

    if (type === 'url') {
      return this.sanitizer.bypassSecurityTrustResourceUrl(content);
    } else if (type === 'html') {
      return this.sanitizer.bypassSecurityTrustHtml(content);
    }
  }

}
