import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {first, map} from 'rxjs/operators';
import {environment} from "../../../environments/environment";


@Injectable()
export class PersonalService {

  private isWriter: BehaviorSubject<any>;
  public $sendIsWriter: Observable<any> = new Observable<any>();
  private baseApiUrl: string = `${environment.apiBaseUrl}/user-post`;

  public getPost: Observable<any> = new Observable<any>();
  private getPostBehSubj: Subject<any> = new Subject<any>();

  private photoRecords: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient) {
    this.isWriter = new BehaviorSubject<any>(true);
    this.$sendIsWriter = this.isWriter.asObservable();

    this.getPost = this.getPostBehSubj.asObservable();
  }

  /***************************/
  setIsWriter(data: any) {
    this.isWriter.next(data);
  }

  setPhotoRecords(record: any) {
    this.photoRecords.next(record);
  }

  getPhotoRecords(): Observable<any> {
    return this.photoRecords.asObservable();
  }

  deletePost(post: any, user_id: number): Observable<any>{
     const httpOptions = {
      headers: new HttpHeaders()
    };
    return this.http.post<any>(`${this.baseApiUrl}/delete`, post, httpOptions)
      .pipe(
        first(),
        map(postResult => {
          setTimeout(()=>{
            this.getPosts(user_id).subscribe((w) => {
            });
          },333);

          return postResult;
        })
      );
  }

  getPosts(user_id): Observable<Post[]> {

    return this.http.get<any>(`${this.baseApiUrl}/${user_id}`).pipe(map(data => {
      if (data['method'] === 'success') {
        this.getPostBehSubj.next(data['userPost']);
        this.setPhotoRecords(data['userPost'].filter(photo => photo.type === 'photo'));
        return data['userPost'];
      } else {
        return false;
      }
    }));
  }
  /*********************получить посты подписок*****************************/
  getSubscribePost(user_id:any): Observable<Post[]>{

    return this.http.get<any>(`${environment.apiBaseUrl}/subscribe/post/${user_id}`).pipe(map(data => {
      if (data['method'] === 'success') {
        this.getPostBehSubj.next(data['userPost']);
        this.setPhotoRecords(data['userPost'].filter(photo => photo.type === 'photo'));
        return data['userPost'];
      } else {
        return false;
      }
    }));
  }


  /******************новый подьзовательский пост****************************/
  newPost(newPostData: any, user_id: any) {
    const httpOptions = {
      headers: new HttpHeaders()
    };
    return this.http.post<any>(`${this.baseApiUrl}`, newPostData, httpOptions)
      .pipe(
        first(),
        map(postResult => {
          this.getPosts(user_id).subscribe((w) => {
          });
          return postResult;
        })
      );
  }

  /*
   *
   * @param error
   */
  private handleError(error: HttpErrorResponse) {
    // TODO: seems we cannot use messageService from here...
    let errMsg = (error.message) ? error.message : 'Server error';
    if (error.status === 401) {
      window.location.href = '/';
    }
    return Observable.throw(errMsg);
  }
}


export class Post {
  user_id: number;
  photo: string;
  id: number;
  type: string;
  date: string;
  description: string;
  dataList: any;
  id_post: number;
}


