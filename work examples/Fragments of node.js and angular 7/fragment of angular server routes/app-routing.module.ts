import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthRegContainerComponent} from './authorization/auth-reg-container.component';
import {AuthGuard} from './_guards';
import {MainComponent} from './home/main/main.component';
import {RegisterComponent} from "./authorization/register/register.component";
import {AuthComponent} from "./authorization/auth/auth.component";

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
  },
  {
    path: 'auth', component: AuthRegContainerComponent,
    children: [
      {
        path: 'sign-up', component: RegisterComponent
      },
      {
        path: 'log-in', component: AuthComponent
      },
      {
        path: '', redirectTo: 'log-in', pathMatch: 'full'
      },
    ]
  },
  {
    path: 'personal-area',
    loadChildren: './personal/personal-module/personal-module.module#PersonalModuleModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'profile-worker/:user_id',
    loadChildren: './views-for-users/views-for-users-module/views-for-users-module.module#ViewsForUsersModuleModule',
    // canActivate: [AuthGuard]
  },
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
