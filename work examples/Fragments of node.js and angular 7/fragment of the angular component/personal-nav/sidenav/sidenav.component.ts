import {Component, ElementRef, HostListener, Input, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {User} from '../../../../services/models';
import {AuthenticateUserService} from '../../../../services/auth/authenticate-user.service';
import {NavigationEnd, Router} from '@angular/router';
import {SidenavStateService} from '../../../../services/personal_area/sidenav-state.service';
import {last, switchMap, takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {ChatRoomsService} from "../../../../services/messages/chat-rooms.service";
import {Observable, Subscription, timer} from "rxjs";

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(600)),
    ]),
  ]
})
export class SidenavComponent implements OnInit, OnDestroy {

  @ViewChild('menu') menu: ElementRef;
  @ViewChild('overlay') overlay: ElementRef;
  isOpen = false;
  isClickMenu = false;
  currentState = false;
  countIncoming: Observable<any>;
  friendComing: Observable<any>;

  public user: User = new User();

  constructor(private router: Router,
              private renderer: Renderer2,
              private authService: AuthenticateUserService,
              private sidenavStateService: SidenavStateService,
              private chatRoomsService: ChatRoomsService) {
  }


  sideEvent(event) {
    this.currentState = false;
  }

  ngOnInit() {

    this.friendComing = this.chatRoomsService.friendsIbs;


    this.countIncoming = this.chatRoomsService.unreadObs;

    try {

      this.authService.currentUser
        .pipe(takeUntil(componentDestroyed(this)))
        .subscribe(user => {
          let uS = JSON.parse(localStorage.getItem('currentUser'));
          this.user = uS;

          for (let key in user) {
            if (uS) {
              if (key === 'photoLink') {
                this.user[key] = uS[key] + '?decache=' + Math.random();

              } else {
                this.user[key] = user[key];
              }
            }
          }
        });

    } catch (e) {

    }


    this.router.events
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((val) => {
        if (val instanceof NavigationEnd) {

            if (this.isOpen) {
              this.currentState = false;
              this.isClickMenu = true;
              this.renderer.addClass(this.menu.nativeElement, 'sidenav-menu--active');
            } else {
              this.isClickMenu = true;
              this.renderer.removeClass(this.menu.nativeElement, 'sidenav-menu--active');
            }
          }

      });

  }

  backDoor() {
    this.sidenavStateService.sendNav('info');
  }

  @Input()
  set sideOut(val: any) {
    this.isOpen = (this.menu.nativeElement.offsetLeft === -260);
    if (val > 0) {
      if (this.isOpen) {
        this.currentState = true;
        this.isClickMenu = true;
        this.renderer.addClass(this.menu.nativeElement, 'sidenav-menu--active');
      } else {
        this.isClickMenu = false;
        this.renderer.removeClass(this.menu.nativeElement, 'sidenav-menu--active');
      }
    }
  }

  onClickOutside() {
    if (this.isClickMenu === true) {
      this.isClickMenu = false;
    } else {
      this.renderer.removeClass(this.menu.nativeElement, 'sidenav-menu--active');
      this.currentState = false;
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

  ngOnDestroy(): void {
  }
}
