import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../services/models';
import {NavigationEnd, Router} from '@angular/router';
import {map, takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '@w11k/ngx-componentdestroyed';
import {SidenavStateService} from "../../../services/personal_area/sidenav-state.service";
import {AuthenticateUserService} from "../../../services/auth/authenticate-user.service";


@Component({
  selector: 'app-personal-nav',
  templateUrl: './personal-nav.component.html',
  styleUrls: ['./personal-nav.component.scss']
})
export class PersonalNavComponent implements OnInit, OnDestroy {
  currentUser: User;
  isOpen = 0;
  public isNotAuthState;

  constructor(private router: Router,
              private sidenavStateService: SidenavStateService,
              private authService: AuthenticateUserService) {

  }

  ngOnInit() {



    this.authService.currentUser
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(value => {
        let authFlag = false;
        try {
          let usC = JSON.parse(localStorage.getItem('currentUser'));
          if (usC === null) {
            authFlag = false;
          } else {
            authFlag = true;
          }
        } catch (e) {
          authFlag = false;
        }
        this.isNotAuthState = authFlag;
      });
    this.router.events
      .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((val) => {
        if (val instanceof NavigationEnd) {
          this.sendSNav();
        }
      });

  }

  sendSNav() {
    this.isOpen = this.isOpen + 1;
  }

  ngOnDestroy(): void {
  }
}
