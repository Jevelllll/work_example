let moment_js_date_ls = require('moment-timezone');
let time_now = moment_js_date_ls.tz('Europe/Kiev').unix();
const time_at = moment_js_date_ls.unix(time_now).format('YYYY-MM-DD HH:mm:ss');
const userPostModel = require('./../models/userPostModel');
const express = require('express');
const router = express.Router();

/***************http://[**]/api/user-post*****
 bodyParams {
 user_id int(11)
 type varchar(45)
 description text
 dataList longtext
 created_at timestamp
 }
 /****************/
router.post('/', (req, res) => {

  if (req.body) {

    let dataRequest = req.body;
    req.getConnection(async (err, connection) => {
      if (err) {
        res.status(404).json({errorMessage: 'not Connected DB!', method: 'not_success'}).end();
        next();
      } else {
        let postModel = new userPostModel(connection);
        let commonData = '';
        if (dataRequest.data) {

          commonData = JSON.parse(dataRequest.data);
          commonData['update_at'] = time_at;
          commonData['type'] = 'text';
          /********************разбор контента images********************************************/
          if (req.files) {

            let files = req.files['files[]'];

            let content = [];
            let encodeRFC5987ValueChars = (str) => {
              return encodeURIComponent(str)
                .replace(/['()]/g, escape)
                .replace(/\*/g, '%2A')
                .replace(/%(?:7C|60|5E)/g, unescape);
            };
            if (Array.isArray(files)) {
              files.map(async value => {

                let name = `${postModel.makeid()}.${value.name.split('.')[1]}`;
                content.push({content: name});

                try {
                  await postModel.saveFile(value.data, name, 'user_data_post/images/');
                } catch (e) {
                }
              });
              try {
                content = JSON.stringify(content);
                commonData['dataList'] = content;
                commonData['type'] = 'photo';
              } catch (e) {
              }
            } else {
              let name = `${postModel.makeid()}${files.name}`;
              content.push({content: name});
              try {
                await postModel.saveFile(files.data, name, 'user_data_post/images/');
              } catch (e) {
              }
              try {
                content = JSON.stringify(content);
                commonData['dataList'] = content;
                commonData['type'] = 'photo';
              } catch (e) {
              }
            }
          }
          try {
            let newVar = await postModel.newPost(commonData);
            if (newVar.affectedRows > 0) {
              res.status(200).json({successMessage: 'success_request', method: 'success',}).end();
            } else {
              res.status(404).json({successMessage: 'not_success_request', method: 'not_success'}).end();
            }
          } catch (e) {
            res.status(404).json({successMessage: 'not_success_request', method: 'not_success'}).end();
          }
        }
      }
    });
  } else {
    res.status(404).json({errorMessage: 'not body params', method: 'not_success'}).end();
    next();
  }
});

/*****************************удалить посты пользователя http://[**]/api/user-post/id:user***************************/
router.post('/delete', (req, res, next) => {
  if (req.body) {

    let dataRequest = req.body;

    req.getConnection(async (err, connection) => {
      if (err) {
        res.status(404).json({errorMessage: 'not Connected DB!', method: 'not_success'}).end();
        next();
      } else {
        let postModel = new userPostModel(connection);
        if (dataRequest.datas) {
          try {
            if (Array.isArray(dataRequest.datas)) {
              dataRequest.datas.map(async content => {
                let firstKey = content[Object.keys(content)[0]];
                firstKey = firstKey.substr(firstKey.lastIndexOf('/') + 1);
                try {
                  await postModel.removePictures(firstKey);
                }catch (e) {
                }
              });
            }
          } catch (e) {
          }
        }
        try {
          let resp = await postModel.removeMyPost(dataRequest.id_post);
          if (resp.affectedRows > 0) {
            res.status(200).json({successMessage: 'success_request', method: 'success',}).end();
          } else {
            res.status(404).json({successMessage: 'not_success_request', method: 'not_success'}).end();
          }
        }catch (e) {
          res.status(404).json({successMessage: 'not_success_request', method: 'not_success'}).end();
        }
      }

    });
  }else {
    res.status(404).json({errorMessage: 'not body params', method: 'not_success'}).end();
    next();
  }
});

/*****************************получить посты пользователя http://[**]/api/user-post/id:user***************************/
router.get('/:user_id', (req, res, next) => {
  if (req.params) {
    try {
      let user_id = Number.parseInt(req.params['user_id'].replace(/\D+/g, ""));
      if (Number.isInteger(user_id)) {
        req.getConnection(async (error, connection) => {

          if (error) {
            res.status(404).json({errorMessage: 'not Connected DB!', method: 'not_success'}).end();
            next();
          } else {
            let postModel = new userPostModel(connection);
            let postData = await postModel.getPostForUserId(user_id, req.headers.host);
            res.status(200).json({
              successMessage: 'success_request',
              method: 'success',
              userPost: postData
            }).end();
          }
        });
      } else {
        res.status(404).json({errorMessage: 'user is integer', method: 'not_success'}).end();
        next();
      }
    } catch (e) {
      res.status(404).json({errorMessage: 'server error', method: 'not_success'}).end();
      next();
    }
  } else {
    res.status(404).json({errorMessage: 'empty_data', method: 'not_success'}).end();
    next();
  }
});
// router.put();

module.exports = router;
