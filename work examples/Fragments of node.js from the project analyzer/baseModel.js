const fs = require('fs'),
    request = require('request'),
    moment_js_date_ls = require('moment-timezone'),
    time_now = moment_js_date_ls.tz('Europe/Kiev').unix(),
    time_at = moment_js_date_ls.unix(time_now).format('YYYY-MM-DD HH:mm:ss'),
    cheerio = require('cheerio');

module.exports = class BaseModel {

    constructor(connection = null) {

        this.phantom = require('phantom');
        this.time_at = time_at;
        this.connetcion = (connection) ? connection : false;
        this.cheerio = cheerio;
       // this.dbP = 'heroku_05aca3a85560f03';
		 this.dbP = 'db_olrep';

        // this.dbP = 'af313538_generat';
    }

    async query(sql, obj_array) {
        return new Promise((resolve, reject) => {
            if (this.connetcion) {
                if (obj_array === undefined) {
                    this.connetcion.query(sql, (error, result) => {
                        result ? resolve(result) : reject(error);
                    });
                } else {
                    this.connetcion.query(sql, obj_array, (error, result) => {
                        result ? resolve(result) : reject(error);
                    });
                }
            } else {
                resolve({success: false, msg: 'not_connected'})
            }
        });
    }

    async readFile(path, opts = 'utf8') {
        return new Promise((res, rej) => {
            fs.readFile(path, opts, (err, data) => {
                if (err) rej(err);
                else res(data)
            })
        });
    }

    request(val) {
        let req = async (value) =>
            new Promise((resolve, reject) => {
                request(value, (error, response, data) => {
                    if (error) reject(error);
                    else resolve(data)
                })
            });
        return req(val);
    }
}