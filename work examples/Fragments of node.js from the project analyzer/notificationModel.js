let BaseModel = require('./baseModel'),
    TelegramBot = require('telegrambot'),
    TOKEN = '******',
    moment_js_date_ls = require('moment-timezone'),
    time_now = moment_js_date_ls.tz('Europe/Kiev').unix(),
    time_at = moment_js_date_ls.unix(time_now).format('YYYY-MM-DD HH:mm:ss'),
    foundModel = require('./foundM');

class notificationModelM extends BaseModel {
    constructor(connection) {
        super(connection);
    }

    async sendMe() {

         let foundM = new foundModel(this.connetcion),
             notSentFoundList = await foundM.notSentFoundList();

         console.log(notSentFoundList);
//         (notSentFoundList.length === 0) return false;


        return new Promise((resolve, reject) => {

               let bot = new TelegramBot(TOKEN),
               fullSendingList = [],
                 notificationBot = async (resultForm, img, foundId) => {
                     return new Promise((resolve, reject) => {
                         bot.invoke('sendPhoto', {
                               //  chat_id: '-392849903',
                                chat_id: '-263268544',
                                 caption: resultForm,
                                 photo: (img)? img: 'http://udr-krg.kz/wp-content/uploads/2017/03/picture_not_available.png'
                             },
                             function (err, message) {
                                 if (err) resolve({success: false, id: foundId});
                                 resolve({success: true, id: foundId});
                             })
                     });
                 };

               if (Array.isArray(notSentFoundList)) {
                 notSentFoundList.map((foundL) => {
                      notificationModelM.textFormatting(foundL, (resultFormatting) => {
                          fullSendingList.push(notificationBot(resultFormatting, foundL['img'], foundL['id']));
                      });
                     let resultFormatting =  notificationModelM.textFormatting(foundL);
                     fullSendingList.push(notificationBot(resultFormatting, foundL['img'], foundL['id']));
                 });

                 Promise.all(fullSendingList).then((result) => {

                     let indicatorUpdate = [];
                     if (Array.isArray(result)) {
                         result.map((res) => {
                             if (res.success) {
                                 indicatorUpdate.push(foundM.editFound(res.id, '1'));
                             }
                         });
                     }
                     Promise.all(indicatorUpdate)
                         .then((responseUpdate) => {
                             resolve({success: true, msg: responseUpdate.length})
                         });
                 });
             }

        });
    }

    static textFormatting(data) {
        let strFull = '';
        // const countElement = [
        //     `${data['href']}`, "\n",
        //     `${data['name']}`, "\n",
        //     `Цена: ${data['coast']}грн.`];

        strFull += `${data['href']}`;
        strFull += `\n`;
        strFull += `${data['name']}`;
        strFull += `\n`;
        strFull += `\`Цена: ${data['coast']}грн.\``;
        return strFull;
    }
}

module.exports = notificationModelM;