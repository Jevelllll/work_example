let BaseModel = require('./../baseModel'), KeyModel = require('./../keywordsM'),
    OLX_URL = 'https://www.olx.ua/', cheerio = require("cheerio"),
    moment_js_date_ls = require('moment-timezone'),
    time_now = moment_js_date_ls.tz('Europe/Kiev').unix(),
    time_at = moment_js_date_ls.unix(time_now).format('YYYY-MM-DD HH:mm:ss'),
    needle = require('needle');

class ParseModel extends BaseModel {

    constructor(connection) {
        super(connection);
        this.tableName = `${this.dbP}.tbl_found`;
    }

    /****************************************main control start*************************************/
    async mainOperation(start = false) {

        let keyModelS = new KeyModel(this.connetcion);
        return new Promise(((resolve, reject) => {
            keyModelS.getAllKAndPatten().then((kAndPatten) => {
                let listParse = [];
                kAndPatten.map((item) => {
                    let url = ParseModel.linkFormation(item);
                    // console.log(item.id_keywords);
                    // console.log(item);
                    listParse.push(this.sendDataFunc(url, item, start, item['skip']));
                });
                return new Promise(((resolve, reject) => {
                    let data = [];
                    Promise.all(listParse).then((commonResult) => {
                        if (Array.isArray(commonResult)) {
                            commonResult.map((cR) => {
                                if (Array.isArray(cR)) {
                                    cR.map((item) => {
                                        if (item !== false)
                                            data.push(item);
                                            console.log(item);
                                    });
                                }
                            });
                        }
                        resolve(data);
                    });
                }))
                    .then((cR) => {
                        let checkSql = `SELECT COUNT(*) AS orderCount FROM ${this.tableName} WHERE href = ? and id_keyw = ? LIMIT 1;`;
                        let query = (sql, obj_array) => new Promise((resolve, reject) => {
                            this.connetcion.query(sql, obj_array, (error, result) => {
                                result ? resolve(result) : reject(error);
                            });
                        });
                        let dataNotInsert = [];
                        cR.map((item) => {
                            let prePush = async (item) => {
                                let rowElement;
                                let exec = await query(checkSql, [item.href, item.id_keyw]);
                                try {
                                    rowElement = exec[0]['orderCount'];
                                } catch (e) {
                                    console.log(e);
                                }
                                let isNotInsert = (0 === Number.parseInt(rowElement));
                                if (isNotInsert) {
                                    return await item;
                                }
                                return false;
                            };
                            dataNotInsert.push(prePush(item));
                        });
                        Promise.all(dataNotInsert).then(found => {
                            if (Array.isArray(found)) {
                                this.valLust(found, (result, key) => {
                                    let sendStatus = {
                                        success: true,
                                        message: null,
                                        last_insert_id: null,
                                        method: 'found'
                                    };
                                   console.log(result.length);
                                    // result.map(item =>console.log(item));
                                    if (result.length === 1) {
                                        let sqlOneInsert = `INSERT INTO ${this.tableName} SET ?;`;
                                        this.connetcion.query(sqlOneInsert, result[0], function (err, results) {
                                            console.log(err);
                                            console.log(result);
                                            sendStatus.message = `affected_rows_${results.affectedRows}`;
                                            sendStatus.last_insert_id = result.insertId;
                                            result ? resolve(sendStatus) : reject(err);
                                        });
                                    }
                                     else if (result.length > 1) {
                                    
                                         let sql = `INSERT INTO ${this.tableName} ${key} VALUES ?`;
                                         this.connetcion.query(sql, [result], function (err, result) {
                                             console.log(result);
                                             sendStatus.message = `affected_rows_${result.affectedRows}`;
                                             sendStatus.last_insert_id = result.insertId;
                                             result ? resolve(sendStatus) : reject(err);
                                         });
                                     } else {
                                         sendStatus.message = 'not_change';
                                         sendStatus.success = false;
                                         resolve(sendStatus);
                                     }
                                });
                            }
                        });
                    });
            });
        }));
    }

    valLust(value, cd) {
        let valueList = [], key = '';
        value.forEach((item, index) => {
            if (item === false) {
            } else {
                let edvList = new Array();
                let strObj = '(';
                for (let prop in item) {
                    strObj += ('`' + prop + '`,')
                    edvList.push(item[prop]);
                }
                strObj = strObj.slice(0, -1);
                strObj += ')';
                key = strObj;
                valueList.push(edvList);
            }
        });
        cd(valueList, key);
    }

    /****************************************parse url*************************************/
    /****************************************start: true=> is_send = '0' or '1'*************************************/
    async sendDataFunc(url, id_keywords, start_flag = false, skip) {

        id_keywords = id_keywords['id_keyword'];

        let pattern = [];

        try {
            pattern = JSON.parse(skip);
        } catch (e) {
        }

        let dataOLX = [];
        // instance = await this.phantom.create(),
        // page = await instance.createPage(),
        // status = await page.open(url),
        // content = await page.property('content'),
		
        
		try{
			let content = await needle('get', url);
        let $ = cheerio.load(content.body);
			     $('table#offers_table>tbody').last().each(async function (i, el) {
            $(el).find('tr.wrap').each(async function (j, item) {
                let coast = $(item).find('td.wwnormal>div>p.price>strong')
                        .text()
                        .replace(/[^-0-9]/gim, '').trim(),
                    intermediate_container = $(item).find('td[valign="top"]');
                let name = $(intermediate_container).find('a>strong').text().trim(),
                    url = $(intermediate_container).find('a').attr('href').trim(),
                    imgHref = $(item).find('img.fleft').attr('src'),
                    intermediateTitle = '';

                /**********handle exceptions by word rules**********/
                let data_js = {};
                if (pattern) {
                    pattern.map(items => {
                        intermediateTitle += '(' + items + ')|';
                    });
                    intermediateTitle = intermediateTitle
                        .substring(0, intermediateTitle.length - 1);
                    let pat = new RegExp(intermediateTitle, "ig");


                    if (name.search(pat) === -1 || pattern.length === 0) {

                        data_js = {
                            id_keyw: id_keywords,
                            name: name,
                            coast: (isNaN(parseInt(coast)))? 0: parseInt(coast),
                            href: url,
                            find_time: time_at,
                            img: imgHref,
                            is_send: (Boolean(start_flag) === true) ? '1' : '0'
                        };
                    } else {
                        data_js = false;
                    }
                } else {
                    data_js ={
                        id_keyw: id_keywords,
                        name: name,
                        coast: (isNaN(parseInt(coast)))? 0: parseInt(coast),
                        href: url,
                        find_time: time_at,
                        img: imgHref,
                        is_send: (Boolean(start_flag) === true) ? '1' : '0'
                    };
                }

                dataOLX.push(data_js);
            });
            await Promise.resolve();
            // await instance.exit();

        });
		}catch(e) {
			
		}
   
        if (dataOLX.length > 0) {
            return await dataOLX
        } else {
            return false;
        }
    }

    /****************************************delete Found*************************************/
    async deleteFound() {
        let sql = `TRUNCATE ${this.tableName};`;
        return this.query(sql);
    }


    /****************************************linkFormation*************************************/
    static linkFormation(item) {

        let encodeRFC5987ValueChars = (str) => {
                return encodeURIComponent(str)
                    .replace(/['()]/g, escape)
                    .replace(/\*/g, '%2A')
                    .replace(/%(?:7C|60|5E)/g, unescape);
            },
            form_defis = (str) => {
                return encodeRFC5987ValueChars(str.toString()
                    .trim()
                    .replace(/\s+/g, " ")
                    .replace(/ /g, '-')
                    .toLowerCase());
            };
        if (item.name_keyword.length) {
            let priceMin = '', priceMax = '', commonPrice = '', category = '', list = '', state = '';
            if (item['price_start'] !== null && item['price_start'] !== '' && item['price_start'] !== '0') {
                priceMin = `search%5Bfilter_float_price%3Afrom%5D=${item['price_start']}`;
            }
            if (item['price_end'] !== null && item['price_end'] !== '' && item['price_end'] !== '0') {
                priceMax = `search%5Bfilter_float_price%3Ato%5D=${item['price_end']}`;
            }
            if (priceMin !== '' && priceMax !== '') {
                commonPrice = `?${priceMin}&${priceMax}`;
            } else if (priceMin === '' && priceMax !== '') {
                commonPrice = `?${priceMax}`;
            } else if (priceMin !== '' && priceMax === '') {
                commonPrice = `?${priceMin}`;
            }

            if (item['state'] !== null && item['state'] !== '' && item['state'] !== 'default') {
                if (commonPrice.length > 3) {
                    state = '&search%5Bfilter_enum_state%5D%5B0%5D=' + item['state'];
                } else {
                    state = '?search%5Bfilter_enum_state%5D%5B0%5D=' + item['state'];
                }
            }

            if (item['category'] !== null && item['category'] !== '') {
                category = `${item['category']}/`;
            } else {
                list = 'list/';
            }

            let commonQueryUrl = `${OLX_URL}${list}${category}q-${form_defis(item.name_keyword)}/${commonPrice}${state}`;
            console.log(commonQueryUrl);
            return commonQueryUrl;
        }
        return false;
    }
}

module.exports = ParseModel;