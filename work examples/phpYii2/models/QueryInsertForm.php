<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.09.2017
 * Time: 11:02
 */


namespace api\modules\v1\models\query;

use api\modules\v1\controllers\BaseController;
use api\modules\v1\models\query\auto\TblAutoAdditionalInfo;
use api\modules\v1\models\query\auto\TblQueryAuto;
use api\modules\v1\models\query\electronics\ElectronicsAdditionalInfo;
use api\modules\v1\models\query\electronics\QueryElectronics;
use api\modules\v1\models\query\electronics\TblElectronicsAdditionalInfo;
use api\modules\v1\models\query\electronics\TblQueryElectronics;
use api\modules\v1\models\query\property\QueryProperty;
use api\modules\v1\models\Users;
use Yii;
use yii\base\ErrorException;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\db\Exception;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;
use yii\filters\Cors;




class QueryInsertForm extends Model
{

    private $additionalQuery;
    private $additionalInfo;


    public $query_id;


    public function __construct($dataObject, $config = [])
    {

        $this->additionalQuery = $dataObject[0];
        $this->additionalInfo = $dataObject[1];

        parent::__construct($config);
    }


//  нужны правки .........................................
    public function insertAllData($insertData)
    {
        header("Access-Control-Allow-Origin: *");
        $header = Yii::$app->request->getHeaders();
        $client_id = $header->get('User-Id');

        $insertData["query_user_id"] = $client_id;
        $insertData["query_confirm_time"] = date("Y-m-d H:i:s");;
        $messageSuccess = "Success! ";
        $connection =Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            if ($insertData["query_tariff"] == null && $insertData["query_user_id"] == null){

                 $this->json_output(400, ["message" => "All fields are empty"]);
                $transaction->rollBack();
            } else {


                if ($this->additionalQuery->load($insertData, '') && $this->additionalQuery->save(true)) {
                    $messageSuccess .= "Filled in search fields";
                    $insertData['ainfo_id_query'] = $this->additionalQuery->query_id;

                } else {
                    $this->json_output(400, $this->additionalQuery->errors);
                    $transaction->rollBack();
                }

                $countAdditionalField = count($insertData["ainfo_id_additional_fields"]);


                if ($countAdditionalField > 0) {

                    foreach ($insertData['ainfo_id_additional_fields'] as $key => $value) {

                        $this->additionalInfo->isNewRecord = true;
                        $this->additionalInfo->ainfo_id = null;

                        $this->additionalInfo->ainfo_id_query = $insertData['ainfo_id_query'];
                        $this->additionalInfo->ainfo_id_additional_fields = $key;
                        $this->additionalInfo->ainfo_value = $value;

                        if ($this->additionalInfo->save(true)) {
                            $messageSuccess .= " and Additional fields are filled in correctly";
                        }
                    }
                }

                $this->json_output(200, ["message" => $messageSuccess, 'id_query' => $this->additionalQuery->query_id]);
                $transaction->commit();

            }
           } catch (ErrorException $e) {
            $this->json_output(400, ["message" => $e->getMessage()]);
            $transaction->rollBack();
          }


    }



        public function getQueryOnj()
        {

            return $this->additionalQuery;


        }


        public function delete($id_query = null)
        {

            if ($id_query !== null){

                if ($this->additionalQuery instanceof QueryElectronics || $this->additionalQuery instanceof TblQueryAuto || $this->additionalQuery instanceof QueryProperty ) {

                    $queryElectronics = $this->additionalQuery;
                    $staticQuery = $queryElectronics::findOne($id_query);

                    if (empty($staticQuery)) {

                             return $this->json_output(204, ["message" => "Data not found"]);
                    } else {

                        if ($staticQuery->delete()) {

                            $this->json_output(200, ["message" => "Successful deletion"]);

                        } else {

                            $this->json_output(204, ["message" => "Uninstall failed"]);

                        }
                    }
                }

          } else {

                    $this->json_output(200,"Заглушка массового удаления");

            }



        }



        public function updateInfoFields($query_id, $insert_data = [])
        {

            $header = Yii::$app->request->getHeaders();
            $client_id = $header->get('User-Id');


            $connection =Yii::$app->db;
            $transaction = $connection->beginTransaction();


            if ($this->additionalQuery instanceof QueryElectronics && $this->additionalInfo instanceof  ElectronicsAdditionalInfo
                      ||$this->additionalQuery instanceof TblQueryAuto && $this->additionalInfo instanceof TblAutoAdditionalInfo){

                $additionalInfo = $this->additionalInfo;
                $additionalQuery = $this->additionalQuery;

                if ($query_id !== null && !empty($query_id) || $query_id != 0){

                    try{
                            if (empty($insert_data)){

                                $this->json_output(400, ["message" => "All fields are empty"]);
                                $transaction->rollBack();

                            } else {

                                $activeQuery = $additionalQuery::find();

                                $activeRecordQuery = $activeQuery
                                    ->where(['query_id' => $query_id])
                                    ->where(['query_user_id' => $client_id])
                                    ->one();

                                if ($activeRecordQuery->load($insert_data, '')) {

                                    if ($activeRecordQuery->update()) {

                                        $this->json_output(200, ["message" => "success query update!"]);

                                    } else {

                                        $this->json_output('400', $activeRecordQuery->errors);

                                    }

                                }

                                $countInfo = count($insert_data['ainfo_id_additional_fields']);

                                if ($countInfo > 0) {

                                    foreach ($insert_data['ainfo_id_additional_fields'] as $key => $value) {

                                        $additionalInfo->isNewRecord = true;
                                        $one = $additionalInfo::find()
                                            ->where(['ainfo_id_query' => $query_id])
                                            ->where(['ainfo_id_additional_fields' => $key])
                                            ->one();

                                        $one->ainfo_value = $value;

                                        if ($one->update(false)) {

                                            $this->json_output(200,['message' => "Additional fields are filled in correctly"]);

                                        }
                                    }
                                }
                            }

                        $transaction->commit();
                    }catch (Exception $e){

                        $this->json_output(400, ["message" => $e->getMessage()]);
                        $transaction->rollBack();
                    }

                }

            }

        }


//    Вынести в отдельный класс
    public function json_output($statusHeader,$responseData)
    {

        $responseData = array_merge(["status" => $statusHeader], $responseData);

        $response = \Yii::$app->response;
        $response->statusCode = $statusHeader;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $response->data = $responseData;

    }




}