<?php

namespace api\modules\v1\models\query\electronics;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "tbl_model_electronics".
 *
 * @property integer $id
 * @property integer $tbl_brand_electronics_id
 * @property string $model_electronics
 */
class TblModelElectronics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_model_electronics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tbl_brand_electronics_id'], 'integer'],
            [['model_electronics'], 'string', 'max' => 20],
            [['model_electronics'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tbl_brand_electronics_id' => 'Tbl Brand Electronics ID',
            'model_electronics' => 'Model Electronics',
        ];
    }

    public static function getModelFindByBrandId($id = null)
    {

        if ($id !== null ){

            $activeQuery = self::find()
                ->where(['tbl_brand_electronics_id' => $id])
                ->asArray()
                ->all();

        } else {

            throw new NotFoundHttpException();

        }

        return $activeQuery;

    }
    public static function getModel($id = null)
    {

        if ($id !== null ){

            $activeQuery = self::find()
                ->where(['id' => $id])
                ->one();

        } else {

            $activeQuery = self::find()
                ->all();

        }

        return $activeQuery;

    }
}
