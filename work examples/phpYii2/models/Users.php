<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.08.2017
 * Time: 11:59
 */

namespace api\modules\v1\models;
use api\modules\v1\models\query\auto\TblQueryAuto;
use api\modules\v1\models\query\electronics\QueryElectronics;
use api\modules\v1\models\query\property\QueryProperty;
use \yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $user_id
 * @property string $password
 * @property string $phone
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $city
 * @property string $last_login
 * @property integer $status
 * @property string $secret_key
 * @property string $secret_expired_at
 * @property string query_count
 * @property string token_fcm
 */


class Users extends ActiveRecord
{

    const STATUS_DELETED = 3;
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $query_count;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }


    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['user_id'];
    }

    /**
     * Define rules for validation
     */


    public function load($data, $formName = null)
    {

        if (isset($data['password'])){
            $data['password'] = Yii::$app->getSecurity()->generatePasswordHash($data['password']);
        }
        
        $dataLoad =  parent::load($data, $formName);

        return  $dataLoad;

    }

    public function rules()
    {

        return
            [
            [['password', 'phone','email', 'first_name', 'last_name', 'city'], 'required'],
                [['phone', 'email'],'unique'],
                [['query_count'], 'integer'],
                [['token_fcm'], 'string']
            ];

    }
    public function fields()
    {

        $fields = parent::fields();
        $fields['query_count'] = function () {
            return $this->get_query_count($this->user_id);
        };

        return $fields;
    }
    public static function get_query_count($user_id) {

        $qw_auto = TblQueryAuto::find()->where(['query_user_id' => $user_id])->count();
        $qw_property = QueryProperty::find()->where(['query_user_id' => $user_id])->count();
        $qw_electronics = QueryElectronics::find()->where(['query_user_id' => $user_id])->count();

        return $qw_auto + $qw_electronics + $qw_property;
    }

    public function getUsers($id = null)
    {
        $dataUser = [];

        if ($id == null){

            $users = self::find()->all();
            foreach ($users as $item => $value){

                if (isset($value['password'])){
                    unset($value['password']);
                }
                $dataUser[$item] = $value;
            }
        }else{

            $dataUser = self::findOne($id);
            unset($dataUser['password']);
            unset($dataUser['secret_key']);
            unset($dataUser['secret_expired_at']);
            unset($dataUser['last_login']);
        }
        return $dataUser;
    }

    public static function findByUsername($usermail)
    {
        return static::findOne(['email' => $usermail, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findBySecretKey($key)
    {
//        if (!static::isSecretKeyExpire($key))
//        {
//            return null;
//        }
        return static::findOne([
            'secret_key' => $key,
        ]);
    }


    public function generateSecretKey() {
        $length = 16;
        $chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $str = "";

        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        $this->secret_key = $str;
    }

    public function secret_expired_at(){



    }


    public function removeSecretKey()
    {
        $this->secret_key = null;
    }

    public function setPassword($password)
    {
        $this->password = md5($password);
    }
}