<?php

namespace api\modules\v1\models\query\electronics;

use api\modules\v1\models\query\cities\Cities;
use api\modules\v1\models\query\regions\Regions;
use api\modules\v1\models\tariff\Tariff;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "query_electronics".
 *
 * @property integer $query_id
 * @property integer $query_user_id
 * @property integer $query_tariff
 * @property integer $query_activated
 * @property integer $paid_for
 * @property string $query_category_electronics
 * @property integer $query_electronics_brand
 * @property integer $query_electronics_model
 * @property string $query_type_electronics
 * @property string $query_confirm_time
 * @property string $query_price_min
 * @property string $query_price_max
 * @property string $query_currency
 * @property string $query_address
 * @property integer $query_city
 * @property integer $query_region
 *
 * @property ElectronicsAdditionalInfo[] $electronicsAdditionalInfos
 */
class QueryElectronics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'query_electronics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query_user_id', 'query_tariff', 'query_activated','paid_for',
                'query_electronics_brand', 'query_electronics_model',
                'query_tariff_start', 'query_tariff_end',
                'query_city', 'query_region'], 'integer'],
            [['query_category_electronics', 'query_type_electronics', 'query_price_min', 'query_price_max', 'query_currency'], 'number'],
            [['query_confirm_time'], 'string'],
            [['query_address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'query_id' => 'Query ID',
            'query_user_id' => 'Query User ID',
            'query_tariff' => 'Query Tariff',
            'query_section' => 'Query Section',
            'query_category_electronics' => 'Query Category Electronics',
            'query_electronics_brand' => 'Query Electronics Brand',
            'query_electronics_model' => 'Query Electronics Model',
            'query_type_electronics' => 'Query Type Electronics',
            'query_confirm_time' => 'Query Confirm Time',
            'query_price_min' => 'Query Price Min',
            'query_price_max' => 'Query Price Max',
            'query_currency' => 'Query Currency',
            'query_address' => 'Query Address',
            'query_city' => 'Query City',
            'query_region' => 'Query Region',
            'paid_for' => 'Paid_for'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElectronicsAdditionalInfo()
    {
        return $this->hasMany(ElectronicsAdditionalInfo::className(), ['ainfo_id_query' => 'query_id']);
    }
    public function getQuery_category_electronics()
    {
        return $this->hasMany(TblCategoryElectronics::className(), ['id' => 'query_category_electronics']);
    }
    public function getQuery_electronics_brand()
    {
        return $this->hasMany(TblBrendElectronics::className(), ['id' => 'query_electronics_brand']);
    }
    public function getQuery_electronics_model()
    {
        return $this->hasMany(TblModelElectronics::className(), ['id' => 'query_electronics_model']);
    }
    public function getQuery_electronics_type()
    {
        return $this->hasMany(TblTypeElectronics::className(), ['id' => 'query_type_electronics']);
    }
    public function getTariff()
    {
        return $this->hasMany(Tariff::className(), ['id' => 'query_tariff']);
    }
    public function getRegion()
    {
        return $this->hasMany(Regions::className(), ['id_region' => 'query_region']);
    }
    public function getCity()
    {
        return $this->hasMany(Cities::className(), ['id_city' => 'query_city']);
    }

//    private function check_an_update($user_id) {
//        $check_active_update = self::find()
//            ->andWhere(['query_user_id'=> $user_id])
//            ->all();
//
//        foreach ($check_active_update as $item) {
//            if ($item->query_tariff_end < strtotime("now")) {
//                $item->query_activated = 0;
//                $item->save(false);
//            }
//        }
//    }

    public function getSimpleDataElQuery() {

        $final_data = [];
        $query = new Query();
        $header = Yii::$app->request->getHeaders();
        $user_id = $header->get('User-Id');

        $query->select([
            self::tableName().'.query_id',
            self::tableName().'.query_activated',
            'tbl_type_electronics.type_electronics',
            'tbl_category_electronics.category_electronics',
            'tbl_brend_electronics.brend_electronics',
            'tbl_model_electronics.model_electronics',
            self::tableName().'.query_tariff_start',
            self::tableName().'.query_tariff_end',
            'tbl_tarif.*'])
            ->from(self::tableName())
            ->join('LEFT OUTER JOIN', 'tbl_type_electronics',
                'tbl_type_electronics.id = '. self::tableName().'.query_type_electronics')
            ->join('LEFT OUTER JOIN', 'tbl_category_electronics',
                'tbl_category_electronics.id = '. self::tableName().'.query_category_electronics')
            ->join('LEFT OUTER JOIN', 'tbl_brend_electronics',
                'tbl_brend_electronics.id = '. self::tableName().'.query_electronics_brand')
            ->join('LEFT OUTER JOIN', 'tbl_model_electronics',
                'tbl_model_electronics.id = '. self::tableName().'.query_electronics_model')
            ->join('LEFT OUTER JOIN', 'tbl_tarif',
                'tbl_tarif.id = '. self::tableName().'.query_tariff')
            ->where(['query_user_id' => $user_id]);

        $command = $query->createCommand();
        $data = $command->queryAll();

        foreach ($data as $key => $val) {

            $final_data[$key]['query_id'] = (int) $val['query_id'];
            $final_data[$key]['type_electronics'] =  $val['type_electronics'];
            $final_data[$key]['category_electronics'] = $val['category_electronics'];
            $final_data[$key]['brend_electronics'] = $val['brend_electronics'];
            $final_data[$key]['model_electronics'] = $val['model_electronics'];
            $final_data[$key]['query_activated'] = (int) $val['query_activated'];
            $final_data[$key]['query_tariff_start'] = (int)$val['query_tariff_start'];
            $final_data[$key]['query_tariff_end'] = intval($val['query_tariff_end']);
            $final_data[$key]['tariff_type_id'] = (int)$val['tariff_type_id'];
            $final_data[$key]['sum_day'] = (int)$val['sum_day'];
            $final_data[$key]['tariff_name'] = $val['tariff_name'];
            $final_data[$key]['count_sms'] = (int)$val['count_sms'];
            $final_data[$key]['discount'] =(double) $val['discount'];

        }

        return $final_data;
    }



    public function getAllFieldsQuery($id = 0)
    {

        $dataQW = [];

        $dataFind = self::find();

        $header = Yii::$app->request->getHeaders();

        $user_id = $header->get('User-Id');

//       $this->check_an_update($user_id);

        if ($id != 0){

            $data = $dataFind

                ->where(['query_id'=> $id])
                ->andWhere(['query_user_id'=> $user_id])
                ->with('electronicsAdditionalInfo',
                    'query_category_electronics',
                    'query_electronics_brand',
                    'query_electronics_model',
                    'query_electronics_type')->asArray()->one();
            $dataQW['fieldQuery'] = $data;

        } else {

            $data = $dataFind->asArray()
//                ->select(['query_id'])
                ->where(['query_user_id'=> $user_id])
                ->with(['electronicsAdditionalInfo',
                    'query_category_electronics' => function ($query) {
                        $query->select(
                            [   'id',
                                'tbl_type_elecronics_id',
                                'category_electronics'
                            ]
                        );
                    },
                    'query_electronics_brand',
                    'query_electronics_model',
                    'query_electronics_type',
                    'tariff',
                    'region',
                    'city'
                    ])
                ->asArray()
                ->all();
            foreach ($data as $key => $value){
                $dataQW[$key] = $value;

            }

        }

        return $dataQW;
    }

}
