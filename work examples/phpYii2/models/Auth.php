<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.08.2017
 * Time: 13:19
 */

namespace api\modules\v1\models;
use Codeception\Module\Yii2;
use \yii\db\ActiveRecord;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

/**
 * This is the controller class.
 *
 * @property integer $user_id
 * @property string $token
 * @property string updated_at
 * @property string expired_at
 * @property string created_at
 * @property boolean state
 *
 */



class Auth extends ActiveRecord
{

    var $client_service = "frontend-client";
    var $auth_key       = "???";



    public $remember_me = false;

    public static function tableName()
    {
        return 'users_authentication';
    }


    public static function primaryKey()
    {
        return ['id'];
    }

    public function check_auth_client()
    {

        $header = Yii::$app->request->getHeaders();

        $client_service = $header->get('Client-Service');

        $auth_key = $header->get('Auth-Key');

        if ($client_service == $this->client_service && $auth_key == $this->auth_key){
            return true;
        } else {

            $items = array('status' => 401, 'message' => 'Unauthorized');
            return $items;
        }

    }

    public static function auth()
    {

        $header = Yii::$app->request->getHeaders();

        $user_id = $header->get('User-Id');
        $token = $header->get('Authorization');



        $expired_at = self::find()
            ->select(["expired_at","state"])
            ->where(["user_id" => intval($user_id)])
            ->AndWhere(["token" => $token])
            ->one();



        if ($expired_at->expired_at == NULL){

            throw new UnauthorizedHttpException("Unauthorized.");
        } else {


            if ($expired_at->expired_at < date('Y-m-d H:i:s')){
                throw new UnauthorizedHttpException("Your session has been expired.");
            }else{

                $update_at = date('Y-m-d H:i:s');

                if ($expired_at->state == 1){

                    $time_remember = "+12 month";

                } else {

                    $time_remember = "+12 hours";
                }

                $expired_at = date("Y-m-d H:i:s", strtotime($time_remember));


                $updateTime = self::findOne([
                        "user_id" => $user_id,
                        "token"   => $token
                        ]);
                $updateTime->expired_at = $expired_at;
                $updateTime->updated_at = $update_at;
                $updateTime->update(false);

                return [
                    "status"  => 200,
                    "message" => "Authorized."
                ];
            }
        }

    }


    public function login($user_phone, $user_password = null, $token_fcm = null)
    {
//        return $token_fcm;
        $daTaPass = (new Query())
            ->select(['password', 'user_id', 'first_name', 'last_name'])
            ->from('users')

            ->where(['phone' => $user_phone])
            ->one();



        if ($daTaPass['password'] == null){

            throw new NotFoundHttpException("Username not found.");
//            return [
//              'status' => 204,
//              'message' => 'Username not found.'
//            ];
        } else {
            $md5Pass = $daTaPass['password'];
            $user_id = $daTaPass['user_id'] ;
            $user_first_name = $daTaPass['first_name'];
            $user_last_name = $daTaPass['last_name'];
        }



//        need md5



//            if ($md5Pass == md5($user_password))
            if (Yii::$app->getSecurity()->validatePassword($user_password, $md5Pass))
            {

                $last_login = date('Y-m-d H:i:s');
                $token = Yii::$app->security->generateRandomString();

                if ($this->remember_me == true){

                    $time_remember = "+12 month";

                } else {

                    $time_remember = "+720 minutes";
                }
                $expired_at = date("Y-m-d H:i:s", strtotime($time_remember));


                $users = Users::findOne($user_id);
                $users->last_login = $last_login;
                $users->token_fcm = $token_fcm;
                $users->save(false);

                $login = new self();
                $login->token = $token;
                $login->expired_at = $expired_at;
                $login->created_at =   date("Y-m-d H:i:s");
                $login->user_id = $user_id;
                $login->state = $this->remember_me;
                $login->save(false);

                return array(
                    'status' => 200,
                    'message' => 'Successfully_login',
                    'id' => $user_id,
                    'first_name' => $user_first_name,
                    'last_name' => $user_last_name, 'token' => $token
                    );

            }else{
                throw new NotFoundHttpException("Wrong password.");

            }

    }

        public function logout()
        {
            $header = Yii::$app->request->getHeaders();

            $user_id = $header->get('User-Id');
            $token = $header->get('Authorization');

            $delete_auth = self::find()
                ->where(["user_id" => intval($user_id)])
                ->where(["token" => $token])
                ->one();
            $delete_auth->delete();

            return array('status' => 200,'message' => 'Successfully_logout.');

        }


}