<?php

namespace api\modules\v1\models\query\electronics;

use Yii;

/**
 * This is the model class for table "electronics_additional_fields".
 *
 * @property string $afields_id
 * @property string $afields_type
 * @property string $afields_value
 * @property string $afields_standard
 *
 * @property ElectronicsAdditionalInfo[] $electronicsAdditionalInfos
 */
class ElectronicsAdditionalFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'electronics_additional_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['afields_id'], 'required'],
            [['afields_id', 'afields_type', 'afields_value', 'afields_standard'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'afields_id' => 'Afields ID',
            'afields_type' => 'Afields Type',
            'afields_value' => 'Afields Value',
            'afields_standard' => 'Afields Standard',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElectronicsAdditionalInfos()
    {
        return $this->hasMany(ElectronicsAdditionalInfo::className(), ['ainfo_id_additional_fields' => 'afields_id']);
    }
}
