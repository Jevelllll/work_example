<?php

namespace api\modules\v1\models\query\electronics;

use Yii;

/**
 * This is the model class for table "electronics_additional_info".
 *
 * @property integer $ainfo_id
 * @property integer $ainfo_id_query
 * @property string $ainfo_id_additional_fields
 * @property string $ainfo_value
 *
 * @property ElectronicsAdditionalFields $ainfoIdAdditionalFields
 * @property QueryElectronics $ainfoIdQuery
 */
class ElectronicsAdditionalInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'electronics_additional_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ainfo_id_query', 'ainfo_id_additional_fields'], 'required'],
            [['ainfo_id_query'], 'integer'],
            [['ainfo_id_additional_fields', 'ainfo_value'], 'string', 'max' => 255],
            [['ainfo_id_additional_fields'], 'exist', 'skipOnError' => true, 'targetClass' => ElectronicsAdditionalFields::className(), 'targetAttribute' => ['ainfo_id_additional_fields' => 'afields_id']],
            [['ainfo_id_query'], 'exist', 'skipOnError' => true, 'targetClass' => QueryElectronics::className(), 'targetAttribute' => ['ainfo_id_query' => 'query_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ainfo_id' => 'Ainfo ID',
            'ainfo_id_query' => 'Ainfo Id Query',
            'ainfo_id_additional_fields' => 'Ainfo Id Additional Fields',
            'ainfo_value' => 'Ainfo Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAinfoIdAdditionalFields()
    {
        return $this->hasOne(ElectronicsAdditionalFields::className(), ['afields_id' => 'ainfo_id_additional_fields']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAinfoIdQuery()
    {
        return $this->hasOne(QueryElectronics::className(), ['query_id' => 'ainfo_id_query']);
    }
}
