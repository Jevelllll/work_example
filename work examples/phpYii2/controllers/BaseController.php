<?php


namespace api\modules\v1\controllers;

use api\modules\v1\models\ActivationRenewal;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\StringHelper;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;

abstract class BaseController extends ActiveController
{
    const API_RIA_KEY = '???';
    const COUNT_DISCOUNT = 30;

    public function json_output($statusHeader, $responseData)
    {

        $response = \Yii::$app->response;
        $response->statusCode = $statusHeader;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $responseData;

    }

    public function randomPassword()
    {
        $alphabet = '1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    private function parseStringRequest($order_id)
    {
        $path = 'api\\modules\\v1\\models\\query\\';
        $str = $order_id;
        $explode = StringHelper::explode($str, '/', true);
        $date_accos = [
            'query_id' => $explode[0],
            'query_category' => ($explode[1] === 'auto') ? $path . $explode[1] . '\\TblQuery' .
                ucfirst($explode[1]) : $path . $explode[1] . '\\Query' . ucfirst($explode[1])
        ];
        return $date_accos;
    }

    protected function checkLiqDate($order_id, $activated = false)
    {
        date_default_timezone_set("UTC");
        $empty_class = new \stdClass();
        $amount = 0;
        if (isset($order_id)) {

            $date_accos = $this->parseStringRequest($order_id);

            $class_name = $date_accos['query_category'];

            try {
                $objJCl = Yii::createObject([
                    'class' => $class_name,
                ]);

                $query_tariff_start = 0;
                $query_tariff_end = 0;

                $empty_class = $objJCl::find()
                    ->with('tariff')
                    ->where(['query_id' => $date_accos['query_id']])
                    ->one();

                if ($activated) {
                    $actRenewal = ActivationRenewal::find()->where(['query_id' => $date_accos['query_id']])
                        ->andWhere(['query_category' => $class_name])
                        ->one();
                    $query_tariff_start = $actRenewal->query_tariff_start;
                    $query_tariff_end = $actRenewal->query_tariff_end;
                } else {
                    $query_tariff_start = $empty_class->query_tariff_start;
                    $query_tariff_end = $empty_class->query_tariff_end;

                }


                if ($query_tariff_start !== null && $query_tariff_end !== null) {

                    $sum_day = (int)$empty_class->tariff[0]->sum_day;
                    $discount = (double)$empty_class->tariff[0]->discount;
                    $start = $query_tariff_start;
                    $end = $query_tariff_end;

                    $gmdateSt = gmdate("Y-m-d", $start);
                    $gmdateEn = gmdate("Y-m-d", $end);

                    $dates=date_create($gmdateSt);
                    $datee=date_create($gmdateEn);

                    $diff=date_diff($dates, $datee);
                    $count = (int)$diff->format("%R%a days");

                   $amount = $this->countPercentSum($count, $discount, $sum_day);

                   if (isset($amount) || !empty($amount)) {


                        if (!$activated) {
                            ActivationRenewal::mainUpdate($date_accos['query_id'], $class_name,strtotime($gmdateSt),strtotime($gmdateEn));
                            $empty_class->query_tariff_start = strtotime($gmdateSt);
                            $empty_class->query_tariff_end = strtotime($gmdateEn);
                            $empty_class->update(false);
                        }
                   }

                }

            } catch (InvalidConfigException $e) {

                return $this->json_output($e->getCode(), ['message' => $e->getMessage()]);
            }

        }
            return $amount;
    }

    private function countPercentSum($count_day, $percent, $sum_day)
    {

        $dividet = 0;
        $general = 0;
        if (is_numeric($count_day) && is_double($percent) && isset($count_day)
            || !empty($count_day) && isset($percent) || !empty($percent)) {

            if ($count_day > 0) {

                if ($sum_day > 0) {
                    if ($count_day < self::COUNT_DISCOUNT) {

                        for ($start_index = 0; $start_index < $count_day; $start_index++) {

                            $general += $sum_day;

                        }
                    } else {

                        for ($start_index = 0; $start_index < $count_day; $start_index++) {

                            $dividet += $sum_day * $percent;
                            $general += $sum_day;

                        }
                    }
                }
            }
        }
        return intval(round($general - $dividet));
    }

    public function update_active($order_id, $query_tariff_start = null, $query_tariff_end = null)
    {
        date_default_timezone_set("UTC");

        $date_accos = $this->parseStringRequest($order_id);
        $class_name = $date_accos['query_category'];
        $objJCl = Yii::createObject([
            'class' => $class_name,
        ]);

        $date_content = $objJCl::find()
            ->where(['query_id' => $date_accos['query_id']])
            ->one();
        if ($query_tariff_start === null && $query_tariff_end === null) {
            $activeRenewal = ActivationRenewal::find()->where(['query_id' => $date_accos['query_id']])->andWhere(['query_category' => $class_name])->one();

            $date_content->query_tariff_start = $activeRenewal->query_tariff_start;
            $date_content->query_tariff_end = $activeRenewal->query_tariff_end;
            $date_content->query_activated = true;
            $date_content->paid_for = true;
            $date_content->save(false);
            return ['new'];
        } else {
            $gmdateSt = date("Y-m-d", $query_tariff_start);
            $gmdateEn = date("Y-m-d", $query_tariff_end);
            return ActivationRenewal::mainUpdate($date_accos['query_id'], $class_name, strtotime($gmdateSt), strtotime($gmdateEn) );

        }
        return false;
    }
}